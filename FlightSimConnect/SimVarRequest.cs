﻿namespace FlightSimConnect
{
    public class SimvarRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public double Value { get; set; }

        public string Unit { get; set; }
    };
}
