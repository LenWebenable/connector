﻿namespace FlightSimConnect
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connect_button = new System.Windows.Forms.Button();
            this.serialSelectBox = new System.Windows.Forms.ComboBox();
            this.serialBox = new System.Windows.Forms.GroupBox();
            this.closePortButton = new System.Windows.Forms.Button();
            this.openPortButton = new System.Windows.Forms.Button();
            this.baudRateLabel = new System.Windows.Forms.Label();
            this.baudRateSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flightVarGridView = new System.Windows.Forms.DataGridView();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.errorList = new System.Windows.Forms.ListBox();
            this.serialBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightVarGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // connect_button
            // 
            this.connect_button.Location = new System.Drawing.Point(12, 12);
            this.connect_button.Name = "connect_button";
            this.connect_button.Size = new System.Drawing.Size(173, 63);
            this.connect_button.TabIndex = 0;
            this.connect_button.Text = "Connect";
            this.connect_button.UseVisualStyleBackColor = true;
            this.connect_button.Click += new System.EventHandler(this.connect_Click);
            // 
            // serialSelectBox
            // 
            this.serialSelectBox.FormattingEnabled = true;
            this.serialSelectBox.Location = new System.Drawing.Point(112, 30);
            this.serialSelectBox.Name = "serialSelectBox";
            this.serialSelectBox.Size = new System.Drawing.Size(182, 33);
            this.serialSelectBox.TabIndex = 1;
            // 
            // serialBox
            // 
            this.serialBox.Controls.Add(this.closePortButton);
            this.serialBox.Controls.Add(this.openPortButton);
            this.serialBox.Controls.Add(this.baudRateLabel);
            this.serialBox.Controls.Add(this.baudRateSelect);
            this.serialBox.Controls.Add(this.label1);
            this.serialBox.Controls.Add(this.serialSelectBox);
            this.serialBox.Location = new System.Drawing.Point(644, 12);
            this.serialBox.Name = "serialBox";
            this.serialBox.Size = new System.Drawing.Size(300, 179);
            this.serialBox.TabIndex = 2;
            this.serialBox.TabStop = false;
            this.serialBox.Text = "Serial settings";
            // 
            // closePortButton
            // 
            this.closePortButton.Enabled = false;
            this.closePortButton.Location = new System.Drawing.Point(7, 119);
            this.closePortButton.Name = "closePortButton";
            this.closePortButton.Size = new System.Drawing.Size(133, 48);
            this.closePortButton.TabIndex = 4;
            this.closePortButton.Text = "Close port";
            this.closePortButton.UseVisualStyleBackColor = true;
            this.closePortButton.Click += new System.EventHandler(this.closePortButton_Click);
            // 
            // openPortButton
            // 
            this.openPortButton.Location = new System.Drawing.Point(146, 119);
            this.openPortButton.Name = "openPortButton";
            this.openPortButton.Size = new System.Drawing.Size(148, 48);
            this.openPortButton.TabIndex = 4;
            this.openPortButton.Text = "Open port";
            this.openPortButton.UseVisualStyleBackColor = true;
            this.openPortButton.Click += new System.EventHandler(this.openPortButton_Click);
            // 
            // baudRateLabel
            // 
            this.baudRateLabel.AutoSize = true;
            this.baudRateLabel.Location = new System.Drawing.Point(7, 78);
            this.baudRateLabel.Name = "baudRateLabel";
            this.baudRateLabel.Size = new System.Drawing.Size(87, 25);
            this.baudRateLabel.TabIndex = 3;
            this.baudRateLabel.Text = "Baud rate";
            // 
            // baudRateSelect
            // 
            this.baudRateSelect.FormattingEnabled = true;
            this.baudRateSelect.Items.AddRange(new object[] {
            "4800",
            "9600",
            "19200"});
            this.baudRateSelect.Location = new System.Drawing.Point(112, 69);
            this.baudRateSelect.Name = "baudRateSelect";
            this.baudRateSelect.Size = new System.Drawing.Size(182, 33);
            this.baudRateSelect.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "COM port";
            // 
            // flightVarGridView
            // 
            this.flightVarGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.flightVarGridView.Location = new System.Drawing.Point(12, 197);
            this.flightVarGridView.Name = "flightVarGridView";
            this.flightVarGridView.RowHeadersWidth = 62;
            this.flightVarGridView.Size = new System.Drawing.Size(932, 225);
            this.flightVarGridView.TabIndex = 3;
            this.flightVarGridView.Text = "dataGridView1";
            // 
            // disconnectButton
            // 
            this.disconnectButton.Enabled = false;
            this.disconnectButton.Location = new System.Drawing.Point(12, 81);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(173, 63);
            this.disconnectButton.TabIndex = 0;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // errorList
            // 
            this.errorList.FormattingEnabled = true;
            this.errorList.ItemHeight = 25;
            this.errorList.Location = new System.Drawing.Point(12, 430);
            this.errorList.Name = "errorList";
            this.errorList.Size = new System.Drawing.Size(935, 79);
            this.errorList.TabIndex = 4;
            // 
            // MainWindow
            // 
            this.ClientSize = new System.Drawing.Size(968, 522);
            this.Controls.Add(this.errorList);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.flightVarGridView);
            this.Controls.Add(this.serialBox);
            this.Controls.Add(this.connect_button);
            this.Name = "MainWindow";
            this.Text = "Disconnect";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.serialBox.ResumeLayout(false);
            this.serialBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightVarGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button connect_button;
        private System.Windows.Forms.ComboBox serialSelectBox;
        private System.Windows.Forms.GroupBox serialBox;
        private System.Windows.Forms.Label baudRateLabel;
        private System.Windows.Forms.ComboBox baudRateSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openPortButton;
        private System.Windows.Forms.Button closePortButton;
        private System.Windows.Forms.DataGridView flightVarGridView;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.ListBox errorList;
    }
}

