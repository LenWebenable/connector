﻿using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FlightSimConnect
{
    public partial class MainWindow : Form
    {
        public enum DUMMYENUM
        {
            Dummy = 0
        }

        public SimConnect _simConnect { get; set; }
        public SerialPort _serialPort = new SerialPort();
        public ObservableCollection<SimvarRequest> _requests = new ObservableCollection<SimvarRequest>();
        const int WM_USER_SIMCONNECT = 0x0402;

        public Timer _timer { get; set; } = new Timer();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            serialSelectBox.Items.AddRange(SerialPort.GetPortNames());

            _requests = new ObservableCollection<SimvarRequest>
            {
                new SimvarRequest
                {
                    Id = 1,
                    Name = "YOKE X POSITION",
                    Value = 0,
                    Unit = "position"
                },
                new SimvarRequest
                {
                    Id = 2,
                    Name = "YOKE Y POSITION",
                    Value = 0,
                    Unit = "position"
                },
                //new SimvarRequest
                //{
                //    Id = 3,
                //    Name = "ACCELERATION WORLD Z",
                //    Value = 0,
                //    Unit = "GForce"
                //}
            };

            flightVarGridView.DataSource = _requests;

            _timer.Interval = 500;
            _timer.Tick += new EventHandler(onTick);
        }

        protected override void DefWndProc(ref Message m)
        {
            if (_simConnect != null)
            {
                try
                {
                    _simConnect.ReceiveMessage();
                }
                catch (COMException ex)
                {
                    throw ex;
                }
            }
            else
            {
                base.DefWndProc(ref m);
            }
        }

        private void connect_Click(object sender, EventArgs e)
        {
            try
            {
                _simConnect = new SimConnect("SimConnectPanel", this.Handle, WM_USER_SIMCONNECT, null, 0);
                _simConnect.OnRecvOpen += new SimConnect.RecvOpenEventHandler(SimConnect_OnRecvOpen);
                _simConnect.OnRecvQuit += new SimConnect.RecvQuitEventHandler(simConnect_OnRecvQuit);

                _simConnect.OnRecvSimobjectDataBytype += new SimConnect.RecvSimobjectDataBytypeEventHandler(SimConnect_OnRecvSimobjectDataBytype);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Connection to KH failed: " + ex.Message);
            }
        }

        private void onTick(object sender, EventArgs e)
        {
            foreach (SimvarRequest request in _requests)
            {
                _simConnect?.RequestDataOnSimObjectType((DUMMYENUM)request.Id, (DUMMYENUM)request.Id, 0, SIMCONNECT_SIMOBJECT_TYPE.USER);
            }
        }

        private void SimConnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            Debug.WriteLine("SimConnect_OnRecvOpen");
            Debug.WriteLine("Connected to KH");

            connect_button.Enabled = false;
            disconnectButton.Enabled = true;
            _timer.Start();

            foreach (var request in _requests)
            {
                RegisterToSimConnect(request);
            }
        }

        private void simConnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            connect_button.Enabled = true;
            disconnectButton.Enabled = false;

            _timer.Stop();
            _simConnect.Dispose();
            _simConnect = null;
        }

        private void SimConnect_OnRecvSimobjectDataBytype(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {
            Debug.WriteLine("SimConnect_OnRecvSimobjectDataBytype");

            uint iRequest = data.dwRequestID;
            uint iObject = data.dwObjectID;
            double dValue = (double)data.dwData[0];

            Debug.WriteLine($"{iRequest}, {iObject}, {dValue}");

            if (_serialPort.IsOpen)
            {
                _serialPort.Write($"{iRequest}:{dValue}");
            }
        }


        private void openPortButton_Click(object sender, EventArgs e)
        {
            try
            {
                _serialPort.BaudRate = Convert.ToInt32(baudRateSelect.Text);
                _serialPort.PortName = serialSelectBox.Text;
                _serialPort.Open();
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                openPortButton.Enabled = false;
                closePortButton.Enabled = true;
                Debug.WriteLine($"Port {serialSelectBox.Text} Opend!");
            }
            catch
            {
                openPortButton.Enabled = true;
                closePortButton.Enabled = false;
                Debug.WriteLine("Port not opend");
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            Debug.WriteLine(_serialPort.ReadExisting());
        }

        private bool RegisterToSimConnect(SimvarRequest request)
        {
            if (_simConnect != null)
            {
                /// Define a data structure
                _simConnect.AddToDataDefinition((DUMMYENUM)request.Id, request.Name, request.Unit, SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                /// IMPORTANT: Register it with the simconnect managed wrapper marshaller
                /// If you skip this step, you will only receive a uint in the .dwData field.
                _simConnect.RegisterDataDefineStruct<double>((DUMMYENUM)request.Id);

                return true;
            }
            else
            {
                return false;
            }
        }

        private void closePortButton_Click(object sender, EventArgs e)
        {
            _serialPort.Close();
            _serialPort.Dispose();
            Debug.WriteLine("Port close");
        }

        public void Disconnect()
        {
            Debug.WriteLine("Disconnect");
            
            _simConnect.Dispose();
            _simConnect = null;

            _timer.Stop();

            connect_button.Enabled = true;
            disconnectButton.Enabled = false;
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            Disconnect();
        }
    }
}